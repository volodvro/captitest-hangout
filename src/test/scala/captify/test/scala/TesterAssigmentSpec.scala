package captify.test.scala

import captify.test.scala.SparseIterators._
import org.scalatest.FlatSpec
import captify.test.scala.TestAssignment._

/**
 * Created by volodvro on 20.07.16.
 */

class TesterAssigmentSpec extends FlatSpec {

  it should "consist of 3 elements " in {
    val simplestIterator = sampleAfter(iteratorFromOne, 1, 2)
    assert(simplestIterator.toSeq.size == 3)
  }

  it should "return sorted merge of 1 iterator should produce the same output" in {
    val mergedIterators = mergeIterators(Seq(sampleAfter(iteratorFromOne, 1, 2)))
    assert(mergedIterators.toSeq == sampleAfter(iteratorFromOne, 1, 2).toSeq)
  }

  it should "return sorted iterator" in {
    val iterators: Iterator[BigInt] = mergeIterators(Seq(
      sampleAfter(iteratorFromOne, 1, 2),
      sampleAfter(iteratorFromOne, 1, 2),
      sampleAfter(iteratorFromOne, 1, 2))
    )

    iterators.toSeq foreach(println _)

    iterators.toSeq.foldLeft(BigInt(0))((accum, el) => {
      assert(el >= accum)
      el
    })
  }

  it should "generate sparce sequence of 4 elements" in {
    assert((approximatesFor(2, 5, 10) map { case (int, appr) => int}) == ((2 to 5).toSeq map(BigInt(_))))
  }

}